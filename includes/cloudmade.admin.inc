<?php

/**
 * @file
 * This file holds the functions for the main cloudmade Admin settings.
 *
 * @ingroup cloudmade
 */

/**
 * Menu callback; displays the cloudmade module settings page.
 *
 * @see system_settings_form()
 */
function cloudmade_admin_settings() {
  $form = array();
  $form['cloudmade_key'] = array(
    '#type' => 'textfield',
    '#title' => t('CloudMade Developers Key'),
    '#description' => t('In order to use the <a href="@services">CloudMade 
      services</a>, you must <a href="@key">register get a key</a> first.
      ', array(
        '@services' => 'http://developers.cloudmade.com/projects',
        '@key' => 'http://cloudmade.com/register',
      )),
    '#default_value' => variable_get('cloudmade_key', ''),
  );

  // Make a system setting form and return
  return system_settings_form($form);
}
