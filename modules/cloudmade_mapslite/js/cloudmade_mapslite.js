
/**
 * @file
 * This file holds the main javascript API for CloudMade Maps Lite module. 
 * It is responsable for loading and displaying the map.
 *
 * @ingroup cloudmade
 */

/**
 * General namespacing
 */
Drupal.settings.cloudmade = Drupal.settings.cloudmade || {};
Drupal.settings.cloudmade.mapslite = Drupal.settings.cloudmade.mapslite || {};

/**
 * Implements Drupal Behavior.
 *
 * The main function to start building CloudMade Maps Lite maps.
 */
Drupal.behaviors.cloudmadeMapslite = function(context) {
  if (typeof(Drupal.settings.cloudmade.mapslite) === 'object' && 
    Drupal.settings.cloudmade.mapslite.maps && !$(context).data('mapslite')) {
    $('.cloudmade-mapslite-map:not(.cloudmade-mapslite-processed)').each(function() {
      var $thisMap = $(this);
      var mapId = $thisMap.attr('id');
      $thisMap.addClass('cloudmade-mapslite-processed');

      // Begin building map.  Utilize try..catch for error reporting.
      try {
        // Ensure that we have map data to process and that there
        // is a CloudMade API Key.
        if (Drupal.settings.cloudmade.mapslite.maps[mapId] &&
          Drupal.settings.cloudmade.key) {
          // Use API Key and create a map object.
          var cloudmade = new CM.Tiles.CloudMade.Web({
            key: Drupal.settings.cloudmade.key
          });
          var map = new CM.Map(mapId, cloudmade);
  
          // Attach data to map DOM object
          $thisMap.data('mapslite', {
            'map': map, 
            'mapData': Drupal.settings.cloudmade.mapslite.maps[mapId]
          });
  
          // Finally, attach behaviors
          Drupal.attachBehaviors(this);
        }
      }
      catch(e) {
        if (typeof console != 'undefined') {
          console.log(e);
        }
        else {
          // It should not be default to write error output to the screen.
          $thisMap.text(Drupal.t('Error during map rendering: ') + e);
        }
      }
    });
  }
};

/**
 * Function to utilize Drupal's base path if relative path
 * is provided.
 *
 * @param path
 *   Path to process.
 * @param basePath
 *   Path to add if not full or absolute URL.
 */
Drupal.settings.cloudmade.mapslite.relatePath = function(path, basePath) {
  // Check for a full URL or an absolute path.
  if (path.indexOf('://') >= 0 || path.indexOf('/') == 0) {
    return path;
  }
  else {
    return basePath + path;
  }
};