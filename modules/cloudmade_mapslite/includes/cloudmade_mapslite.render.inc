<?php

/**
 * @file
 * cloudmade_mapslite render file
 *
 * This file holds the main functions for rendering a map
 * for the CloudMade Maps Lite module.
 *
 * @ingroup cloudmade
 */

/**
 * Render CloudMade Map Lite map (internal function).
 *
 * @param $name
 *   Map name to render.
 * @return
 *   Themed output for map.
 */
function _cloudmade_mapslite_render($name = '') {
  $map = cloudmade_mapslite_map_load($name);
  if (!empty($map)) {
    cloudmade_mapslite_include();
    
    // Make the name, the map ID, unless it is already
    // set.
    if (!isset($map->data['id'])) {
      $map->data['id'] = $map->name;
    }
    
    // Allow for other modules to alter map.
    // hook_cloudmade_mapslite_map_alter($map_data)
    drupal_alter('cloudmade_mapslite_map', $map->data);
    
    // Add JS
    _cloudmade_mapslite_render_map_js($map->data);
    
    // Create output
    $output = theme('cloudmade_mapslite_map', $map);
    return $output;
  }
  else {
    return FALSE;
  }
}

/**
 * Send map data to JS.
 *
 * @param $data
 *   Array of map data.
 */
function _cloudmade_mapslite_render_map_js($data) {    
  // Send data to JS.  Try to use more efficient json_encode 
  // function, if available
  if (function_exists('json_encode')) {
    drupal_add_js('
      Drupal.settings.cloudmade.mapslite = Drupal.settings.cloudmade.mapslite || {};
      Drupal.settings.cloudmade.mapslite.maps = ' . 
      json_encode(array($data['id'] => $data)) . ';',
      'inline', 'header', TRUE);
  }
  else {
    drupal_add_js(array(
      'cloudmade' => array(
        'mapslite' => array(
          'maps' => array(
            $data['id'] => $data,
          )
        ),
      ),
    ), 'setting');
  }
}