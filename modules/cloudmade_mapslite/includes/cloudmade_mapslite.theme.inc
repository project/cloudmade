<?php

/**
 * @file
 * This file holds the theme functions for cloudmade_mapslite module
 *
 * @ingroup cloudmade
 */

/**
 * Theme function for cloudmade_mapslite map
 */
function theme_cloudmade_mapslite_map($map = NULL) {
  $output = '';
  $data = $map->data;

  // Create output
  $output = '
    <div style="width: ' . $data['width'] . '; height: ' . $data['height'] . ';" id="' . $data['id'] . '" class="cloudmade-mapslite-map cloudmade-mapslite-map-' . $data->name .'">
    </div>
  ';
  return $output;
}